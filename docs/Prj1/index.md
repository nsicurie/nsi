# Projet biographie

L'objectif de ce projet est de concevoir une page web détaillant la vie et les réalisations d'une informaticienne ou d'un informaticien.

## Déroulement

Ce projet et les suivants se déroulent en partie en classe, en partie à la maison (en particulier les recherches documentaires). Il est important pour tout projet de se fixer des objectifs bien découpés, modestes et de les tenir en maîtrisant les délais. 

## Cahier des charges

* On veut une mise en oeuvre de certains éléments html : titre, titres, liens, images, listes, tableaux.
* Un fichier CSS séparé devra être utilisé.
* Si on inclut des éléments d'interactivité en intégrant du code javascript, ce dode sera lui aussi dans un fichier séparé.
* On attend un contenu qui contienne des éléments qui ne soient pas exclusivement extraits de Wikipedia.
* Enfin on veut une illustration de l'apport à l'informatique de la personne choisie, sous forme de quelques lignes de Python.

## Rendu

Le projet devra être rendu sous forme d'une archive (un fichier zip, qui contient plusieurs fichiers en un seul, sous forme compressée), contenant la page html, un fichier css séparé, éventuellement un fihcier js séparé, les images incluses dans la page.

Un premier rendu devra être effectué à mi-projet.