# Le langage javascript

Le langage javascript, inventé en 1996, est un langage de programmation conçu initialement pour s'exécuter dans le contexte d'une page web afin d'apporter de l'interactivité dans cette page.