# Cycle de développement

Le développement d'une page web suit un cycle assez classique en informatique, où on alterne une phase d'édition et une phase de visualisation/test.

Plus spécifiquement, on utilise un éditeur pour travailler sur le fichier html, et après quelques modifications on sauvegarde ce fichier (sans le fermer) et on le charge dans un navigateur pour visualiser le rendu. On retravaille sur le fichier html, on le sauvegarde, on recharge la page dans le navigateur, on recommence.

Idéalement, on dispose sur l'écran la fenêtre de l'éditeur et celle du navigateur côte-à-côte afin d'accélérer ce cycle de développement. Dans l'éditeur on peut voir plusieurs onglets ouverts, en particulier si on travaille sur plusieurs fichiers pages mais aussi lorsqu'on utilise des fichiers css ou des fichiers js. 