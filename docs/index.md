# Données, Algorithmes, Langages, Machines

Ce site fournit des cours, des ressources, des exercices et des travaux pratiques pour la NSI au lycée Curie.

Le découpage de ce cours se fait selon les chapitres suivants :


## Classe de première

* Chapitre 0 Position du problème

  On introduit quelques notions importantes pour la suite, la notion d'algorithme, de programme, de structure de données, de problème et d'instance, les notions de terminaison, de correction et de complexité, et on pratique le Python.

* Chapitre 1 Développement Web

  On s'intéresse à la conception de pages Web et de sites, en utilisant les langages html pour le contenu, css pour la mise en page et la mise en forme, et js pour l'interactivité.

* Projet 1 biographie

  Ce premier projet est l'occasion de mettre en oeuvre les techniques de développement Web vues en cours pour concevoir une page biographique sur une informaticienne ou un informaticien.

* Chapitre 2 Binaire et booléens

  On s'intéresse dans ce chapitre aux écritures binaires et hexadécimales des entiers, puis au type booléens et aux opérations logiques sur les booléenes et les entiers.

* Chapitre 3 Boucles, tableaux et algorithmes linéaires

  Dans ce chapitre, on travaille sur une structure de données essentielle : le tableau. On implémente des algorithmes de complexité linéaire en parcourant des tableaux avec des boucles.

* Chapitre 4 Architecture machine

  On part de la porte logique pour arriver au microprocesseue afin de comprendre comment un programme informatique peut s'exécuter sur un ordinateur.

* Chapitre 5 Dictionnaires et tables

  Le type de données tableau associatif', `dict` en Python, est un type de données très utile en Python, et qui débouche sur la notion de table, des tableaux de dictionnaires ayant les mêmes clés, une table étant une structure qui permet de stocker et traiter des données structurées.

* Chapitre 6 Bash

  Shell, console, terminal, ligne de commande, sont des façons différentes de désigner un même fonctionnement : l'humain tape une commande sur un clavier, presse le bouton entrée, et reçoit une réponse de la machine. On travaille sur le bash, la façon standard d'interagir avec un ordinateur, que ce soit un serveur virtuel, une machine physique, un super ordinateur ou un objet connecté.  

* Projet 2 Serveur en Python

La bibliothèque `Flask` permet de créer simplement en Python des serveurs Web.

* Chapitre 7 Architecture réseau

Les réseaux d'ordinateurs interconnectés avec le protocole TCP/IP forment le réseau internet, sur lequel les données s'éhangent sous forme de paquets d'information. Ce chapitre examine les protocoles mis en oeuvre sur internet, IP, TCP, UDP, mais aussi ARP, DNS, ICMP...

* Prijet 3 Internet des objets

Dans ce projet on utilise des microcontrôleurs connectés à Internet en wifi et suffisamment puissant pour héberger un serveur web permettant de proposer une interface avec les capteurs et actionneurs dont dispose le microcontrôleur. 

* Chapitre 8 Algorithmes gloutons

Un algorithme glouton est un algorithme qui prend une suite de décisions pour arriver à une solution. Il s'agit d'une famille d'algorithmes, qui suivant les problèmes qu'on cherche à résoudre peuvent être optimaux ou ne donner qu'une solution qu'on espère pas trop mauvaise. On examine dans ce chapitre quelques prroblèmes classiques pour lesquels on met en oeuvre des algorithmes gloutons : le rendu de monnaie, le sac à dos, le coloriage de graphes.

* Chapitre 9 Tris et recherche dichotomique

Dans ce chapitre on revient sur le problème du tri et on reprend les algorithmes de tri sélection et de tri insertion déjà rencontrés, avant de voir un très bel algorithme pour finir l'année de première : l'algorithme de la recherche dichotomique. C'est l'occasion aussi de reparler de terminaison, de correction et de complexité.

* Projet 4 Jeu 2D

Afin d'illustrer la diversité des langages informatiques, on utilise dans ce projet le langage javascript pour réaliser un jeu vidéo en deux dimensions à l'intérieur d'une page web, en utilisant les possibilités offertes par le html5. Plus précisément, on commence par s'appuyer sur le tutoriel [Tuto jeu 2D](https://developer.mozilla.org/fr/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript) proposé par la fondation Mozilla, puis on s'intéresse à la façon de déplacer des sprites animés dans un canvas et de gérer les collisions.

## Classe de terminale

* Chapitre 10 ABR, récursivité et POO

Ce chapitre reprend l'algorithme de recherche dichotomique dans une liste triée pour présenter une nouvelle structure de données, l'arbre binaire de recherche. C'est l'occasion d'introduire deux notions importantes en informatique : la récursivité est le fait pour une structure d'être définie à partir d'elle-même, et pour un algorithme ou une fonction de s'appeler lui-même ; la programmation orientée objet (POO) est une façon de programmer dans laquelle on considère des objets et leurs interactions, en particulier Python facilite l'utilisation de ce paradigme.

* Chapitre 11 Bases de données, SGBD, SQL

Dans la continuité du chapitre de première sur les données en tables, on s'intéresse à la notion de base de données, une base de données (BD ou db) étant un ensemble de tables. Un système de gestion de bases de données (relationnelles) est un logiciel qui permet d'accéder à une base de données (en général via le réseau, avec une architecture client/serveur) en assurant la persistance des données, la gestion des accès concurrents, les contraintes  d'intégrité, la sécurisation des accès. Le SQL est le structured query language, ou langage de requête structuré, qu'on utilise pour dialoguer avec le SGBD et effectuer des requêtes.

* Projet 5 Serveur WEB en PHP

L'objectif de ce projet est d'utiliser le langage PHP, qui est un langage exécuté sur un servur web, afin d'implémenter un site web utilisant une base de données. Ce projet est l'occasion de retravailler le html, le css, de reprendre l'architecture client serveur, et de mettre en oeuvre des requêtes SQL.

* Chapitre 12 Structures linéaires

Ce chapitre a pour objet l'étude des structures de données linéaires : piles, files, listes, dictionnaires (tableaux associatifs). L'étude de ces structures est couplée à l'étude des algoritmes classiques opérant sur ces structures ou bien utilisant ces structures, en particulier les parcours d'arbres. C'est l'occasion d'utiliser la POO.

* Chapitre 13 Architecture machine

Dans ce chapitre on s'intéresse au fonctionnement d'un ordinateur, en partant du microprocesseur et de la gestion de la mémoire jusqu'à la gestion des processus, en mettant en évidence le rôle du système d'exploitation (OS operating system) en articulier pour l'ordonnancement des processus.

* Chapitre 14 Arbres

Peu de théorie dans ce chapitre, dans lequel on s'intéresse en toute généralité à la notion d'arbres, pas nécessairement binaires ni de recherche. C'est essentiellement l'occasion de traiter des exercices classiques en faisant intervenir POO, récursivité, parcours et structures linéaires.


* Chapitre 15 Tri fusion

On revoit les tris vus en classe de première, en reprenant les notions de terminaison, de correction et de complexité. On s'intéresse ensuite à un tri basé sur une approche "diviser pour régner", le tri fusion. Ce chapitre est aussi l'occasion de voir quelques autres tris (rapide, bulles, par tas) et de travailler boucles, indices et tableaux.


* Chapitre 16 Routage

On commence par utiliser quelques commandes dsiponibles dans le shell pour travailler sur le réseau, avant de s'intéresser à quelques protocoles de routage sur internet : le protocole RIP (routing information protocol) et OSPF (open shortest path first). C'est l'occasion de modéliser des réseaux par des graphes.

* Chapitre 17 Programmation dynamique

La programmation dynamique est une technique algorithmique qui permet de résoudre des problèmes compliqués en commençanat par résoudre des sous-problèmes plus simples. On met en oeuvre cette idée sur quelques problèmes : parcours dans un tableau, rendu de monnaie, alignement de séquence. On aborde la notion de mémoïsation et on dérécursifie des algorihmes récursifs en les transformant en une version itérative.

* Chapitre 18 Sécurisation des communications

Après un bref tour d'horizon des ocntextes dans lesquels la cryptographie intervient (https, ssh, téléphonie...) nous implémentons quelques algorithmes très simples de cryptographie sur du texte (César, Vigénère). Nous discutons ensuite de la notion de cryptographie à clé symétrique, à clé asymétrique.

* Chapitre 19 Rechcerche textuelle

La recherche d'un mot dans un texte, et plus généralement d'un motif, est un grand classique de l'informatique. Nous implémentons d'abord une recherche naïve d'un mot dans un texte, avant d'implémenter un algorithme beaucoup plus subtil, Boyer-Moore. C'est aussi l'occasion de dcouvrir quelques notions plus avancées : expression régulière, automate, langage rationnel.

* Projet 6 Labyrinthes

Représenter des labyrinthes en Python, créer des labyrinthes, visualiser des labyrinthes (en 2D ou en 3D), et bien sûr sortir des labyrinthes.

* Chapitre 20 Graphes

Un graphe est un ensemble de points (les sommets) reliés par des traits (des arêtes). Cette structure essentielle est à la fois très simple à aborder et très riche en applications et propriétés, la notion de graphe est à l'intersection des Mathématiques discrètes et de l'Informatique théorique.

* Projet 7 Projet libre/ Grand oral

* Chapitre 21 Programmes comme données-Problème de l'arrêt

Des points un peu plus théoriques qui se sont posés dès la naissance de l'Informatique en tant que discipline scientifique spécifique distincte des Mathématiques, et qui touchent à l'essence même de l'Informatique. 
