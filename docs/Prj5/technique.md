## Quelques aspects techniques

Le projet se base sur un serveur **LAMP** : **Linux** comme système d'exploitation, **Apache** comme serveur http, **MySQL** comme SGBD et l'extension **PHP** d'Apache pour créer des pages html dynamiques dont le code PHP sera exécuté sur le serveur pour créer dynamiquement les pages html transmises au client.

Il existe aussi des packages WAMP pour exécuter les serveurs sur la machine locale Windows, mais la politique du conseil régional en ce qui conerne les ouvertures de ports est trop restrictive pour que la solution soit praticable.

### Identifiants et mots de passe

L'utilisatrice **Margaret Hamilton**, née le 17 août 1936, possède :
* un compte utilisateur `hamiltom` avec le mot de passe `19360817` qui lui permet de se connecter en **ssh** sur le vps qui héberge le site `http://nsi.li` ; l'identifiant est composé des sept premières lettres de son nom de famille suivies de son initiale, le mot de passe (initial) est la date de naissance au format jjmmaaaa
* un compte `margareth`  avec le mot de passe `1708` pour se connecter au serveur SQL. L'identifiant est le prénom en entier, suivi de l'initiale du nom de famille, le mot de passe est la date anniversaire (sans l'année) au format jjmm.

### Connexion au site par ssh

Sous windows on utilise PuTTY, et avec un système d'exploitation sérieux (Linux, MacOS, Windows Subsystem for Linux) on utilise ssh dans un shell :

```bash
ssh hamiltom@nsi.li
```

Attention, la WiFi proposée par la région ne permet pas de se connecter en SSH car le port 23 est bloqué. Il faut utiliser un PC fixe, ou bien un partage de connexion sur téléphone portable.

On peut se remettre au `bash` grâce au sympathique jeu **gameshell**, avec :

```bash
./gameshell.sh 
```

On a besoin d'un éditeur, p.ex vim ; pour réviser son utilisation, utiliser

```bash 
vimtutor fr
```

ou bien le site [openvim](openvim.com)


### Connexion à la base de données `MySQL``

A partir de la console bash, on se connecte à MySQL par

```bash
mysql -u margareth -p
```

puis il faut entrer le mot de passe.

Quelques instructions pour vérifier que tout fonctionne :

```SQL
SHOW DATABASES;
```

La base projet ne contient aucune table pour le moment, mais les utilisateurs ont tous les droits sur cette base, en particulier le droit de créer des tables. Pour préciser dans quelle base on travaille, on utilise `USE`.

```SQL
USE projet;
```

Dès que le projet sera un peu précisé, onpourra créer une base spécifique au projet, en donnant les droits aux seuls membres du groupe ; éventuellement dans le groupe on peut désigner un administrateur de la base de données, qui aura davanatge de droits sur la base et qui gérera les droits des autres membres du groupe.

### Le répertoire `public_html`

Dans le répertoire personnel de chaque utilisateur, noté `~` ou `~hamiltom` ou `/home/hamiltom`, il y a un sous répertoire `public_html` qui peut héberger le site personnel de l'utilisateur, via l'URL `http://nsi.li/~hamiltom/`

On va créer une première page très simple, qui sera accessible via `http://nsi.li/~hamiltom/page1.html`,en utilisant vim.

```bash
cd
cd public_html
vim page1.html
```

Dans vim on commence par passer en mode insertion en tapant **i**,
puis on tape le code ci-dessous, et on sauvegarde et quitte avec **Esc :wq**

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width"/>
        <title>Une page simple</title>
    </head>
    <body>
        <h1>Une page d'accueil</h1>
        <p>Cette page ne contient pas grand chose mais il faut bien commencer</p>
    </body>
</html>
```

Vérifier que la page est bien accessible à l'url `http://nsi.li/~hamiltom/page1.html`

### html, css, javascript, formulaires

On peut réviser ses connaissances grâce aux pages [tuto html/css](https://www.w3.org/Style/Examples/011/firstcss.fr.html) et [tuto formulaire](https://developer.mozilla.org/fr/docs/Learn/Forms/Your_first_form)


### git, vscodium

Le logiciel git est un logiciel de gestion de code source, crée par Linus Torvalds initialement pour gérer le code source du noyau de Linux. Il permet de centraliser du code (C, Python, html, php, peu importe) sur des dépots qui peuvent être synchronisés sur plusieurs ordinateurs, par plusieurs utilisateurs, de façon collaborative. La référence essentielle est le [git-book](https://git-scm.com/book/fr/v2)

Un éditeur bien adapté au développement est [VSCodium](https://vscodium.com/), la version libre du logiciel Visual Studio Code de Microsoft. L'éditeur est assez classique mais possède de nombreuses extensions, en particulier pour ouvrir à distance un dépôt git et synchroniser les fichiers édités, ou pour servir des fichiers PHP.

