## Objectif du projet

Créer un site web dynamique en utilisant le langage PHP côté serveur et une base de données mySQL le tout hébergé sur http://nsi.li .

### Organisation

Le projet est mené en groupes de deux ou trois personnes, qui doivent se partager le travail de façon réfléchie, tout en ayant chacun une vue globale sur l'ensemble des aspects du travail.

Une partie du travail a lieu en classe, une partie en dehors de la classe.

Chaque séance en classe commence par une discussion pour se fixer des objectifs pour la séance, et se termine par :

* un bilan : ces objectifs sont-ils atteints ? Entièrement ? Partiellement ? Quels ont été les obstacles ?
* des objectifs pourla séance suivante, en particulier recherches documentaires, travail sur les contenus, débogage en lien avec les difficultés rencontrées pendant la séance,
* un bilan écrit de ces divers points dans un journal de bord (quelques lignes à chaque séance suffisent).

Des points d'étape réguliers, en classe seront l'occasion pour chaque groupe de présenter à l'oral l'état d'avancement du projet au professeur et aux autres groupes.

### Rendu

La fin du projet sera une présentation interactive du site préparé par le groupe, avec un partage d'expérience sur les difficultés rencontrées,les solutions techniques mises en oeuvre, les réussites qui peuvent profiter aux autres élèves,en s'appuyant sur le journal de bord.

### Cahier des charges

Le thème exact du projet est relativement libre, pour peu que soit respecté le cahier des charges :

* le site doit se composer de plusieurs pages, chacune correspondant à un cas d'usage (une *user story*),
* les pages sont conçues en PHP,
* un seul fichier css donne le style de tout le site,
* les pages sont hébergées sur nsi.li dans le répertoire personnel d'un desmembres de l'équipe,
* une base de données hébergée sur le serveur MySQL du site nsi.li sera utilisée par le code php pour le côté dynamique du site,
* des formulaires utilisant la méthode POST doivent être utilisés,
* le code PHP doit mettre en oeuvre des requêtes `INSERT INTO`, `SELECT` , `UPDATE` et `DELETE` ce qui signifie que les cas d'usages traités (*user stories*) doivent prévoir ces différentes manipulations des données stockéees dans la base.

Quelques aspects annexes seront valorisés s'ils sont mis en oeuvre :
* l'authentification des utilisateurs par mot de passe,
* la possibilité de créer un compte,de modifier ses informations en particulier le mot de passe, de supprimer un compte,
* la possibilité de déposer sur le site des blobs (typiquement des images),
* la possibilité de faire envoyer par le site des mails de confirmation.


### Quelques idées en vrac :

* un site de gestion et d'échange de cartes pokemon,
* un site pour gérer un CDI,
* un site pour réserver des séances de cinéma,
* un site d'enquête en ligne,
* un système de micro-messagerie de type twitter,
* un système de gestion de stocks de composants électroniques...

On pourra s'inspirer de la deuxième spécialité pour trouver des idées plaisantes.
