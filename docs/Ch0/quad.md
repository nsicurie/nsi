# Un algorithme quadratique

Une première idée est d'essayer le premier nombre avec tous les autres, puis le deuxième nombre avec tous les autres sauf le premier, puis le troisième ombre avec chacun des nombres à partir de quatrième, et ainsi de suite jusqu'à ce qu'on trouve la some cherchée ou bien jusqu'à ce qu'on ait essayé en vain toutes les possibilités.

## Spécifier une fonction

Une fonction informatique, comme une fonction mathématique, prend un ou plusieurs paramètres, et renvoie une valeur. Spécifier une fonction c'est définir les paramètres, leur signification et leur nature, définir la valeur de retour, et définir le lien entre les paramètres et la valeur de retour. 

Il faut commencer par définir notre objectif : veut-on obtenir les deux nombres qui constituent une solution, ou peut-être leurs indices ? Dans ce cas, que doit renvoyer la fonction lorsqu'il n'y a pas de solution ?

Pour se simplifier la vie, on va écrire une fonction **booléenne**, c'est-à-dire dont la valeur de retour sera `True` si on trouve une solution, ou `False` s'il n'y en a pas.

Les paramètres sont bien sûr un tableau d'entiers qui contient les termes, et un entier qui constitue l'objectif à obtenir. Remarquons qu'on se limite à des entiers ; c'est un choix, mais un choix raisonnable.

Le prototype de notre fonction sera donc `def binome(t,s)` qu'on peut aussi écrire `def binome(t : list, s : int)->bool` si on souhaite préciser explicitement quel est le type des paramètres et le type de la valeur de retour.


## Ecrire du pseudo code et une docstring

Ecrire du pseudo-code est une étape qui consiste à écrire la fonction sous forme de commentaires décrivant l'algorithme, sans trop entrer dans les détails, en particulier sans trop s'intéresser aux structures de données utilisées.

C'est aussi le moment d'écrire une `docstring`, une chaîne de caractères qui contient une description des paramètres, de la valeur de retour, de l'utilité de la fonction, et quelques **cas d'usage**. Cette docstring permet de commenter le code, mais aussi de construire automatiquement une documentation pour le programme, et aussi de tester le bon fonctionnement de la fonction sur les cas d'usage fournis.

Les cas d'usage doivent proposer une bonne **couverture** du code, dans le sens où on doit avoir des cas d'usage variés qui correspondent à des situations variées et si possible exhaustive. Ici on utilisera 3 cas d'usage, un pour lequel la réponse doit être `True`, un pour lequel aucun couple d'éléments du tableau ne donne la somme demandée (la valeur de retour doit donc être `False`) et un derneir qui constitue ce qu'on appelle un **cas-limite** qui en principe n'arrive pas mais pour lequel on veut que la fonction de comporte gracieusement. 

```python
def binome(t,s):
    """
    cette fonction prend un tableau d'entiers t et un entier s
    et renvoie un booléen indiquant si s peut s'écrire comme somme de deux éléments de t
    >>> binome([76,14,50,35,22,29,56,44],100)
    True
    >>> binome([76,14,50,35,22,29,56,44],61)
    False
    >>> binome([],100)
    False
    """
    # pour chaque élément du tableau
        # pour chaque élément suivant cet élément
            # si la somme des deux éléments vaut s
                # renvoyer True
    # renvoyer False 
```

On peut constater que ce pseudo-code est potentiellement très proche du code Python, p.ex `# renvoyer False` se traduit directement en `return False`. Par contre certaines lignes sont un peu plus complexes à traduire, en général lorsque l'accès aux données nécessite un peu de réflexion, p.ex traduire `#pour chaque élément suivant cet élément` nécessite de s'interroger sur la façon dont on va traduire 'élément suivant'.



## Ecrire du code

Il reste à transformer le pseudocode en code, ligne par ligne si le pseudocode est suffisamment détaillé (ce qui n'est pas toujours le cas). On peut commencer par identifier les situations classique, ici on va parcourir un tableau.

!!! note "Parcourir un tableau"

    Il y a essentiellement trois façons de parcourir un tableau :
    
    * par **valeur** `for x in t:`, 
    * par **indice** `for i in range(len(t)):`, 
    * avec `enumerate` sous la forme `for i,x in enumerate(t)`.

Ici on aura besoin dans la boucle intérieure de parcourir le tableau à partir d'un certain rang, donc on va utiliser le parcours par indice.

On n'a pas remis la docstring dans le code ci-dessous. Aussi, on a remplacé les commentaires de psuedo-code par du code, alors qu'on aurait pu laisser ces commentaires.

```Python
def binome(t,s):
    for i in range(len(t)):
        for j in range(i+1,len(t)):
            if t[i]+t[j]==s:
                return True
    return False 
```


## Lancer les tests

L'avantage de la présence de cas d'usage est qu'on peut tester automatiquement 
ces cas d'usage automatiquement grâce à la librairie `doctest`, comme on peut le vérifier dans l'IDE ci-dessous.

On peut retaper la ligne `doctest.testmod()` dans la console pour voir l'affichage produit.

{{ IDE('py/binome.py') }}


## Pourquoi quadratique ?

Il est assez simple de déterminer le nombre de fois où le test d'égalité `t[i]+t[j]==s` sera exécuté par ce programme :

* dans le **meilleur cas** on a `t[0]+t[1]==s` et la fonction renvoie `True`au bout d'une seule comparaison ;

* dans le **pire des cas**, on compare `t[0]+t[j]`à `s` pour $n-1$ valeurs de `j` (de $1$ à $n-1$), puis `t[1]+t[j]`à `s` pour $n-2$ valeurs de `j` (de $2$ à $n-1$), etc jusqu'à comparer `t[n-2]+t[n-1]`à `s` en notant $n$ la longueur du tableau `t` ; en tout cela fait $n-1+n-2+\dots+1 = \dfrac{n(n-1)}2 \approx \dfrac12 n^2$ comparaisons ;

* pour estimer le nombre de comparaisons **en moyenne**, il faut estimer la probabilité de chacune des possibilités, cela complique un peu l'analyse.

En général on travaille avec la complexité dans le pire des cas, qui mesure le nombre maximum d'opérations élémentaires en fonction de la taille de l'entrée, ici le nombre maximal de comparaisons en fonction de la longueur du tableau passé en paramètre.

On dit ici que la complexité est **quadratique** car son expression $\dfrac12 n^2$ est quadratique (un trinôme du second degré).



!!! note Somme de Gauss

    Lorsque le jeune Karl Friedrich Gauss était à l'école primaire, son instituteur Büttner demande aux élèves de calculer la somme $1+2+3+\dots+100$ (espérant sans doute être tranquille quelques minutes). Au bout de quelques secondes, Gauss trouve la réponse.

    De façon générale, posons $S=1+2+3+\dots+n$ où $n$ est un entier fixé, $n=100$ dans l'exercice du professeur Büttner.

    Alors $S=n+(n-1)+(n-2)+\dots+1$, et $S+S=1+n+2+(n-1)+3+(n-2)+\dots+n+1$ donc $2S=n(n+1)$
    
    P.ex $1+2+3+\dots+100=\dfrac{100 \times 101}2=5050$. 
    
    De façon générale $\fbox{1+2+\dots+n=\dfrac{n(n+1)}2}$.