def binome(t,s):
    """
    cette fonction prend un tableau d'entiers t et un entier s
    et renvoie un booléen indiquant si s peut s'écrire comme somme de deux éléments de t
    >>> binome([76,14,50,35,22,29,56,44],100)
    True
    >>> binome([76,14,50,35,22,29,56,44],61)
    False
    >>> binome([],100)
    False
    """
    for i in range(len(t)):
        for j in range(i+1,len(t)):
            if t[i]+t[j]==s:
                return True
    return False

import doctest
doctest.testmod()