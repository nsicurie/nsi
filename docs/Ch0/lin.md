# Un algorithme linéaire

On étudie ici un algorithme plus efficace que la recherche exhaustive quadratique vue précédemment, dans laquelle on essayait tous les couples possibles de valeurs du tableau.

La complexité de cet algorithme est **linéaire**, c'est-à-dire proportionnelle à la taille de l'entrée, à ceci près que cet algorithme nécessite de travailler sur une liste triée.

## Trier les valeurs dans l'ordre croissant

Le problème du tri d'un tableau est un problème classique en informatique, et il existe de nombreux algorithmes plus ou moins efficaces basés sur l'opération élémentaire qui consiste à échanger deux éléments du tableau.

En classe de première, on verra deux algorithmes de tri quadratiques (en nombre d'échanges dans le pire des cas, en fonction de la taille du tableau), mais il existe des tris plus efficaces, en particulier le tri intégré à Python `sort` est **quasi-linéaire** ; c'est une complexité nettement meilleure que quadratique, et un peu moins bonne que linéaire.

Voici un tableau qui donne le nombre d'opérations pour chacune des trois complexité, en fonction de $n$.

|complexité|$n=10$|$n=100$|$n=1000$|$n=10000$|$n=100000$|$n=1000000$|
|:--------:|:----:|:-----:|:------:|:-------:|:--------:|:---------:|
|linéaire $n$|10|100|1000|10000|100000|1000000|
|quasilinéaire $n \times \log(n)$|30|700|10000|140000|1700000|20000000|
|quadratique $n^2$|100|10000|1000000|100000000|10000000000|1000000000000|

Le tri du tableau `t` s'écrira simplement `t.sort()` ; taper `help(list.sort)` dans la console est intéressant.



## Précondition, postcondition


## Dérouler un algorithme dans un tableau




## Des preuves ?

