# Position du problème

## Un problème combinatoire

Peut-on trouver deux nombres parmi 45, 69, 135, 12, 147, 65, 83, 27, 14, 57, 53, 55, 141 dont la somme soit 200 ?

Plus généralement, peut-on trouver dans une liste de nombres deux nombres dont la somme vaut une valeur donnée ?

Et surtout, comment trouver les deux nombres en question ?

On conviendra que la solution est composée des indices des deux nombres.

Attention, en informatique on commence à numéroter à zéro :

|indice|0|1|2|3|4|5|6|7|8|9|10|11|12|
|:--|-|-|-|-|-|-|-|-|-|-|-|-|-|
|valeur|45|69|15|12|147|65|83|27|14|57|53|55|141|


## Problème versus instance du problème

Le **problème** est de façon générale de trouver dans une liste deux nombres dont la somme est fixée.

Le cas proposé dans la première section n'est qu'une **instance** de ce problème, mais qui est bien utile pour pouvoir chercher un algorithme général à partir d'un exemple.

Voici une autre instance du même problème: peut-on ajouter deux nombres parmi 80,76,14,50,35,22,29,56,44, 85 et obtenir 111 ?

On pourrait généraliser à un nombre quelconque de termes ; on parle alors du problème subset sum ou SSS qui est un problème difficile. Nous nous limiterons ici à une somme de deux termes.